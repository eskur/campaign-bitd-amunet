#+TITLE: Names

* Iruvian Names

Ahnav, Aiz, Aniya, Anva, Arkash, Ayan, Darha, D’ruva, Elesh, Elesha, Eva, Evi, Esha, Hakan, Hanesh, Haran, Iana, Iku, Isak, Isha, Izu, Jahan, Jaya, Jin, Kan, Kahan, Kahara, Kavira, Ket, Keta, Kiara, Kos, Kotar, Kotara, Kyra, La’ana, Lasa, Lekat, Lenaya, Lor, Ma’ana, Marek, Mata, Mita, Mo’an, Muhan, Nashala, Nav, Na’ava, Navya, Nek’set, Niru, Ra, Rahan, Rahana, Ro, Ro’an, Ruka, Rukon, Sa’ana, Sarha, Sethla, Sevra, S’rata, Su’ua, Suhin, Syra, Ta’amet, Taji, Tukara, Una, Usa, Useth, Vaha, Vanya, Vara, Vaati, Von, Vondu, Zamira, Zarha, Zora.

* Iruvian Familty Names

Akaana, Anixis, Ankhayat, Ankhuset, Anserekh, Arkhaya, Avrathi, Azu, Daava, D’har, Diala, Hakar, Havran, Jaha, Jayaan, Jeduin, Ka’asa, Kardera, Khara, Khuran, Klevanu, Kutu, Nahjan, Masura, Maat, Nijira, Nur, Nuvket, Saha, Sanaat, Siatu, Siakaru, Siketset, Suresh, Yara, Zayana.

* Iruvian Aliases

Anvil, Arrow, Ash, Axe, Bell, Bird, Blaze, Brass, Breaker, Brick, Broom, Bug, Bull, Cage, Cannon, Cat, Chalk, Cloud, Coal, Cord, Crane, Dagger, Dart, Dove, Dust, Echo, Ember, Fox, Hammer, Hawk, Howler, Jackal, Key, Match, Moon, Moth, Mule, Nail, Needle, Owl, Ox, Pike, Ram, Rasp, Razor, River, Rock, Salt, Scribe, Shimmer, Silk, Silver, Sky, Slate, Smoke, Sparrow, Spinner, Star, Stick, Stitch Thorn, Viper.

* Looks

Man, Woman, Ambiguous, Concealed.

Affable, Athletic, Bony, Bright, Brooding, Calm, Chiseled, Cold, Dark, Delicate, Fair, Fierce, Grimy, Handsome, Huge, Hunched, Languid, Lovely, Open, Plump, Rough, Sad, Scarred, Slim, Soft, Squat, Stern, Stout, Striking, Twitchy, Weathered, Wiry, Worn.

Belt Sash, Cloak, Feathered Cape, Fitted Dress, Fitted Leggings, Half-Cape, Headscarf, Hood & Veil, Hooded Cape, Hooded Coat, Layered Robes, Leathers, Light Jacket, Long Coat, Long Scarf, Loose Silks, Mask & Robes, Rags & Tatters, Scavenged Uniform, Sharp Trousers, Short Cloak, Silk Bodywrap, Silk Kaftan, Simple Tunic, Skirt & Blouse, Soft Boots, Tall Boots, Turban, Vest or Waistcoat, Wide-Legged Trousers, Wide-Brimmed Hat, Work Boots, Work Trousers,

* Emotional Needs
