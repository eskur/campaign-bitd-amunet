**GM/Player**: GM
**Preferred Name**: eskur
**Pronouns**: he/him
**Time Zone**: US Central (UTC-5)
**Times Available**: Saturdays at 17:00, ~3hr sessions
**System(s) of Choice**: Blades in the Dark
**Text/Voice/Video/PBP**: Voice
**Platform**: Discord + Google Sheets, Theatre of Mind
**Experience**: Been playing Forged in the Dark games for a few years now. GM'd one Blades campaign. Also really enjoy Burning Wheel, Mouse Guard, Bastionland, etc.
**Notes**: This will run 5-6 sessions, starting on Oct 17th.
**Players**: 1/4

**The Shadow Bazaars of Betu'at**

U'Duasha, the hearth and heart of Iruvia. Where the ebon sands obscure the past, and the fires burn eternal. They say four Demon Princes reside there, in spires of obsidian crystal lit by celestial flame.
You were quickly climbing the ladder in the city, pulling heists and passing the blame. Then the Hadrakin showed up to collect their cut. Will you ally with them, or turn to another faction for protection? What will you do to survive in this arcane crucible?

**Info**

- I'm looking for proactive players who enjoy working towards character goals, and collaborative play.
- I try to spend as much time as possible in character, and minimize bookkeeping.
- No experience needed.
- Decent microphone required.

Feel free to DM me if you have questions.

https://discord.new/YjFAN5MQYNY8
