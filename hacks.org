#+TITLE: Hacks

* Planning
#+BEGIN_SRC markdown
- Players decide on a target. Discuss and ask questions.

- Each player can pitch a plan type and detail in three sentences or less. Each pitch must offer a new idea.

- If a player has no ideas for a pitch, they can wait until they have heard the other pitches, or yield.

- Players vote for a plan type, majority wins. On a tie, roll to decide.

- Characters discuss the plan, develop the detail.

- Once there is a minimum viable plan and detail, make the engagement roll.

- The GM cuts to the first obstacle. Hard frame.

#+END_SRC

* Homebrew
- The mastery crew upgrade is not available. Each action rating can have a max of 3 dots.
  - All characters have a new XP trigger: _You worked towards or achieved a session goal._ At the start of each session, you can write your own goal. If you work towards it you get 1XP, if you achieve it you get 2XP.
  - Characters are limited to 8 XP gained from triggers per session. Desperate XP does not count towards that limit.
- Instead of entanglement rolls, each heat will introduce a complication that causes a faction/entanglement clock to progress 1 tick. I'll apply heat to clocks once the score in done, and after any _reduce heat_ actions/bonuses are resolved.
