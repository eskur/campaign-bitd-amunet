# Stuff

- stole thief rigging from military

### Notes

#### Crew

# Session

Ahtriv: desperate, fallen in with crew

- signature technique: talking to the right people, charming, trickery

Rahana (Simourv): ex-mil, sapper, prostetic leg,

- sin (defeat), grace (victory),
- makes toys, signature: sparkcraft mask, combine with telekinetics

Szeth (Skitter):

Make contacts with people who can infiltrate Imperial Military

**Rumors around town**

- The I'Rajin are distancing themselves from The Falling Stars
- The A'Tahim and I'Rajin have been competing fiercely in Betu'at.

  - In response, the I'Rajin are pushing into the Eastern District

# session 10/24

szeth: kills falling star lookout to reduce heat, rising moon follower sees and reports back

rahana:

- talks to priest about rising moon
- train
- try to

atriv:

- aquire asset (info source about well)
  - Medhi, orphan manager: Q1, will tell crew about radiant word movements

# opportunities

**Score Target: The Radiant Word**

- Tier: 2
- Location: Radiant Word HQ (under city catacombs)
- Situation: They are holding something that will "ignite the fires of the heavens". Do you let them use it? Do you steal it?
- Plan (obvious attack vector): Get inducted as cultists
  _Extra info_
- Connected Factions: The Falling Stars (enemy, -1), Gualim (on their shit list, -1)
- Plan (subtle or un-intuitive attack vector): Trick them into using the device early
- Secrets or alternate opportunities: Underground tunnels (closed off, who knows about them?)
  _GM Info_
- Payoff: 6 coin + stealing?
  _Ostacles_

# thing

ahtriv's orphans deliver head to guato

shana wants minakshi and irena to be happy family

note: irena has ghost friend (deceased sibling), gave secret info to her, whisper has bound ghost
Ahtriv: investigate Irena's ghost friend by bribing A'Tahim librarian / record keeper

szeth's monstrous: graveyard in middle of the night,

**Enforcers**

- Performance enhancers, hopped up on drugs, blue silk belt-sashes
  **Imperial troops in the market**
