**House Rules**

- PVP must be opt-in on all sides. Having characters argue or butt heats is great (popcorn), but players should be having a good time.
- If score planning devolves into analysis paralysis, I'll start a 5 minute timer. If the timer runs out, I'll roll randomly. The winning player gets to choose the plan from those previously discussed.

**Homebrew**

- The mastery crew upgrade is not available. Each action rating can have a max of 3 dots.
  - All characters have a new XP trigger: _You worked towards or achieved a session goal._ At the start of each session, you can write your own goal. If you work towards it you get 1XP, if you achieve it you get 2XP.
  - Characters can gain up to 8 XP from triggers per session. Desperate XP does not count towards that limit.
- Instead of entanglement rolls, each heat will introduce a complication that causes a faction/entanglement clock to progress 1 tick. I'll apply heat to clocks once the score in done, and after any _reduce heat_ actions/bonuses are resolved.

**Style**

- Investigation
  - During a score you will need to narrate how you investigate / gather info.
    - IE: "I search the room for anything conspicuous. Lifting the paintings, checking for false drawers, under the carpet, etc."
    - "I study the room" is not enough.
    - Rolls will often just be how much time it takes to find something.
  - Outside a score
    - We can have a scene if it seems interesting, otherwise you can roll and describe
  - Usually if a roll fails it means the quality of the info is poor, it took a long time to get, or there was another complication. Rarely will you entirely miss out on info.
- Emphasis on collaboration
  - I'll ask the group for Devil's Bargains on most rolls
  - Players can also offer potential consequences
  - I outline consequences before the roll
  - Players narrate their successes
